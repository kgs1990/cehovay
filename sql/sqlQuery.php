<?php
/**
 * Created by PhpStorm.
 * User: kukuruz-gs
 * Date: 01.04.2018
 * Time: 12:03
 */

$idRec = $_GET["id"];

// выборка данных по сотруднику
$registquery =
    "SELECT 
	код_сотрудника as id,
	карты as card,
	ФИО as Name,
	пол as Sex,
	дата_рожд as birth,
	адрес as Adress,
	полиса as polis,
	удален as del,
	н_пункт as settlementId,
	должность as specialistId,
	отделение as divisionId
	FROM регистр 
	WHERE код_сотрудника = ".$_GET["id"];
$result = sqlsrv_query($connect, $registquery);

while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){

    if (empty($row['card'])){
        $card = 'Не указано';
    }else{
        $card = $row['card'];
    }

    if (empty($row['Name'])){
        $name = 'Не указано';
    }else{
        $name = $row['Name'];
    }

    if (empty($row['Sex'])){
        $sex = 'Не указано';
    }else{
        $sex = $row['Sex'];
    }

    if (empty($row['Adress'])){
        $adress = 'Не указано';
    }else{
        $adress = $row['Adress'];
    }

    if (empty($row['polis'])){
        $policy = 'Не указано';
    }else{
        $policy = $row['polis'];
    }

    if (empty($row['birth'])){
        $birth = 'Не указано';
    }else{
        $birth = $row['birth'];
    }

    if (empty($row['del'])){
        $del = 'Не указано';
    }else{
        $del = $row['del'];
    }

    $settlementId = $row['settlementId'];
    $specialistId = $row['specialistId'];
    $divisionId = $row['divisionId'];
}

if (empty($card)){
    $card = 'Не указано';
}

if (empty($sex)){
    $sex = 'Не указано';
}

if (empty($birth)){
    $birth = 'Не указано';
} else {
    $strBirth = $birth->format('d-m-Y');
}

if (empty($adress)){
    $adress = 'Не указано';
}

if (empty($policy)){
    $policy = 'Не указано';
}

// Список населенных пунктов
$settlListQuery = 'SELECT * FROM СП_прав_нт';
$settlListResult = sqlsrv_query($connect, $settlListQuery);
// Список должностей
$specListQuery =  'SELECT * FROM должности';
$specListResult = sqlsrv_query($connect, $specListQuery);
// Список отделений
$divListQuery = 'SELECT * FROM подразделения';
$divListResult = sqlsrv_query($connect, $divListQuery);

// выбираем отделение
if ($divisionId){
    $divquery = "SELECT * FROM подразделения WHERE Idsubdivision = ".$divisionId;
    $divresult = sqlsrv_query($connect, $divquery);
    while ($row = sqlsrv_fetch_array($divresult, SQLSRV_FETCH_ASSOC)) {
        if ($row['Idsubdivision'] == $divisionId){
            $division = $row['Subdivision'];
        } else {
            $division = 'Не указано';
        }
    }
} else {
    $division = 'Не указано';
}

// выбираем должность
if ($specialistId){
    $specquery =  "SELECT * FROM должности WHERE Idspecializit = ".$specialistId;
    $specresult = sqlsrv_query($connect, $specquery);
    while ($row = sqlsrv_fetch_array($specresult, SQLSRV_FETCH_ASSOC)) {
        if ($row['Idspecializit'] == $specialistId){
            $specialist = $row['specializit'];
        } else {
            $specialist = 'Не указано';
        }
    }
} else {
    $specialist = 'Не указано';
}

// выбираем населенный пункт
if ($settlementId){
    $settlemQuery = "SELECT * FROM СП_прав_нт WHERE name_punktov = ".$settlementId;
    $settlemResult = sqlsrv_query($connect, $settlemQuery);
    while ($row = sqlsrv_fetch_array($settlemResult, SQLSRV_FETCH_ASSOC)) {
        if ($row['name_punktov'] == $settlementId){
            $settlement = $row['TOWN_NAME'];
        } else {
            $settlement = 'Не указано';
        }
    }
} else {
    $settlement = 'Не указано';
}

$vakCodeNames = "SELECT код_вакцины as id,
        название_вакцины as name
        FROM сп_вакцин";
$vakCodeNamesResult = sqlsrv_query($connect, $vakCodeNames);
$vakCodeNamesList = array();
while ($row =  sqlsrv_fetch_array($vakCodeNamesResult, SQLSRV_FETCH_ASSOC)){
    $vakCodeNamesList[] = $row;
}

$denyCodes = "SELECT код_пом_отказа as id,
            название_пометки_отказа as name,
            пометка as flag
            FROM сп_пометок_отказов_медотводов";
$denyCodeResult = sqlsrv_query($connect, $denyCodes);
$denyCodesList = array();
while ($row =  sqlsrv_fetch_array($denyCodeResult, SQLSRV_FETCH_ASSOC)){
    $denyCodesList[] = $row;
}

/**
 * выборка рентгенов по сотруднику
 */
$roentgenQuery = "SELECT код_ф AS id,
                  дата_ф AS rDate,
                  примечание AS comment
                  FROM флюрография
                  WHERE код_сотрудника = {$_GET['id']}";

$roentgenResult = sqlsrv_query($connect, $roentgenQuery);

$roentgenArr = array();
while ($row = sqlsrv_fetch_array($roentgenResult, SQLSRV_FETCH_ASSOC)){
    if (isset($row['rDate'])){
        $row['rDate'] = $row['rDate']->format('Y-m-d');
    }

    $roentgenArr[] = $row;
}

/**
 * выборка вакцие по сотруднику
 */
$vakcina = "SELECT
	код_вакцинации as idVakcina,
	код_сотрудника as idEmploy,
	серия as seria,
	колич as quantity,
	дата as vakDate,
	код_вак as vakCode,
	вид_при as vakType,
	код_пом_отказ as denyCode,
	прим as comment
	FROM вакцины
	WHERE код_сотрудника = " . $_GET["id"];
$vakcinaResult = sqlsrv_query($connect, $vakcina);

$vakTypes = "SELECT код_порядка_в as id,
    наз_порядка_вакцинации as name
    FROM сп_порядок_вакцинации";
$vakTypesResult = sqlsrv_query($connect, $vakTypes);
$vakTypesList = array();
while ($row =  sqlsrv_fetch_array($vakTypesResult, SQLSRV_FETCH_ASSOC)){
    $vakTypesList[] = $row;
}

/**
 * выборка анализов по сотруднику
 */
$query = "SELECT код_анализа as id,
          код_сотрудника as workerId,
          вич as hiv,
          ФАКТОР_ВИЧ as hivFactor,
          гепатит_с as hepatitis_c,
          фактор_с as factor_c,
          гепатит_в as hepatitis_b,
          Фактор_в as factor_b
          FROM анализы
          WHERE код_сотрудника = {$_GET['id']}";

if (isset($query)) {
    $result = sqlsrv_query($connect, $query);
    $analyses = array();
    while ($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC)){
        if (isset($row['hiv'])){
            $row['hiv'] = $row['hiv']->format('Y-m-d');
        }
        if (isset($row['hepatitis_c'])){
            $row['hepatitis_c'] = $row['hepatitis_c']->format('Y-m-d');
        }
        if (isset($row['hepatitis_b'])){
            $row['hepatitis_b'] = $row['hepatitis_b']->format('Y-m-d');
        }

        $analyses[] = $row;
    }
}
