<h2> Редактировать карту </h2>
<?php
$EditBirth = $birth->format('Y-m-d');
echo "<form method='POST'>
	<table class='table'>
		<tr>
			<td>Карта</td>
			<td><input class='PacientTextEdit' type='text' name='card' value='$card'></td>
		</tr>
		<tr>
			<td>ФИО</td>
			<td><input class='PacientTextEdit' type='text' name='name' value='$name'></td>
		</tr>
		<tr>
			<td>Дата рождения</td>
			<td><input class='PacientTextEdit' type='date' name='birth' value='$EditBirth'></td>
		</tr>
		<tr>
			<td>Пол</td>
			<td>
				<select name='sex'><option value='$sex'>$sex</option>
					<option value='МУЖ'>МУЖ</option>
					<option value='ЖЕН'>ЖЕН</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Населенный пункт</td>
			<td>
				<select name='settlement'><option value='$settlementId'>$settlement</option>";
while ($row = sqlsrv_fetch_array($settlListResult, SQLSRV_FETCH_ASSOC)) {
    $settlId = $row['name_punktov'];
    $TownName = $row['TOWN_NAME'];
    echo "<option value='$settlId'>$TownName</option>";
}
echo "</select>
			</td>
		</tr>
		<tr>
			<td>Адрес</td>
			<td><input class='PacientTextEdit' type='text' name='adress' value='$adress'></td>
		</tr>
		<tr>
			<td>Полис</td>
			<td><input class='PacientTextEdit' type='text' name='policy' value='$policy'></td>
		</tr>
		<tr>
			<td>Должность</td>
			<td><select id='spec' name='specialist' onchange='test($idRec)'><option value='$specialistId'>$specialist</option>";
while ($row = sqlsrv_fetch_array($specListResult, SQLSRV_FETCH_ASSOC)) {
    $specId = $row['Idspecializit'];
    $specName = $row['specializit'];
    echo "<option value='$specId'>$specName</option>";
}
echo "</select>
			</td>
		</tr>
		<tr>
			<td>Отделение</td>
			<td><select name='division'><option value='$divisionId'>$division</option>";
while ($row = sqlsrv_fetch_array($divListResult, SQLSRV_FETCH_ASSOC)) {
    $divId = $row['Idsubdivision'];
    $divName = $row['Subdivision'];
    echo "<option value='$divId'>$divName</option>";
}
echo "</select>
			</td>
		</tr>
	</table>	
		<input type='submit' name='action' class='btn' value='Готово'>
	</form>";
?>