<?php
/**
 * Created by PhpStorm.
 * User: kukuruz-gs
 * Date: 09.04.2018
 * Time: 1:54
 */
?>

<form method='post'>
    <h3 id='analysisModalWin'></h3>
    <input type='hidden' id='editAnalysisMode' name="editAnalysisMode">
    <input type='hidden' id="idRowAnalysisHidden" name="idRowAnalysisHidden">
    <table class='table'>
        <tr>
            <td>Уник. номер</td>
            <td><input id="idRowAnalysis" type='text' disabled></td>
        </tr>
        <tr>
            <td>Вич</td>
            <td><input id="editHiv" name="editHiv" type='date'></td>
        </tr>
        <tr>
            <td>Фактор</td>
            <td><input id="editFactorHiv" name="editFactorHiv" type="text"></td>
        </tr>
        <tr>
            <td>Гепатит С</td>
            <td><input id="editHepatitisC" name="editHepatitisC" type='date'></td>
        </tr>
        <tr>
            <td>Фактор</td>
            <td><input id="editFactorC" name="editFactorC" type="text"></td>
        </tr>
        <tr>
            <td>Гепатит B</td>
            <td><input id="editHepatitisB" name="editHepatitisB" type='date'></td>
        </tr>
        <tr>
            <td>Фактор</td>
            <td><input id="editFactorB" name="editFactorB" type="text"></td>
        </tr>

    </table>
    <input type="submit" class='btn' id="editAnalysisDone" name="editAnalysisDone" value="Готово">
</form>