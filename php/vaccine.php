<table class="table table-hover">
    <thead>
    <tr>
        <th>
            <input type='image' id='insertVaccine' onclick="insertVaccineRow()" src='//172.16.0.35/cehovay/img/notebookplus.ico' style='height:16px; width: 16px' value='Добавить'>
        </th>
        <th>у/н</th>
        <th>Вакцина</th>
        <th>Серия</th>
        <th>Кол-во</th>
        <th>Дата</th>
        <th>Тип вакцины</th>
        <th>Пометка</th>
        <th>Примечание</th>
        <th</tr>
    </tr>
    </thead>
    <tbody>
    <?php
    while($row = sqlsrv_fetch_array($vakcinaResult, SQLSRV_FETCH_ASSOC))
    {
        if (!empty( $row['seria'])){
            $seria =  $row['seria'];
        } else {
            $seria = 'Не указана';
        }
        if (!empty( $row['quantity'])){
            $quantity =  $row['quantity'];
        } else {
            $quantity =  'Не указано';
        }
        if (!empty( $row['vakDate'])){
            $vakDate =  $row['vakDate']->format('Y-m-d');
        }
        if (!empty( $row['vakCode'])){
            $vakCode =  $row['vakCode'];
        } else {
            $vakCode = '';
        }
        if (!empty( $row['vakType'])){
            $vakType =  $row['vakType'];
        } else {
            $vakType = '';
        }
        if (!empty( $row['denyCode'])){
            $denyCode =  $row['denyCode'];
        } else {
            $denyCode = 0;
        }
        if (!empty( $row['comment'])){
            $comment =  $row['comment'];
        } else {
            $comment = '';
        }

        $idVaccine = $row['idVakcina'];

        ?>
        <tr id="<?=$row['idVakcina']?>" onclick="editVaccineRow(this.id); deleteRow(this.id, 'vaccine');">
            <td>
                <input type='image' onclick="showModalWin('editVakWin')" src='//172.16.0.35/cehovay/img/217485.png' style='height:16px; width: 16px' value='Редактировать'>
            </td>
            <?php
            echo "<td>".$row['idVakcina']."</td>";
            echo "<td id='vakCode_$idVaccine'><input type='hidden' id='vakCodeVal_$idVaccine' value='$vakCode'>";
            foreach ($vakCodeNamesList as $vakCodeRow) {
                echo ($vakCode == $vakCodeRow['id']."" ? $vakCodeRow['name'] : '');
            }
            echo "</td>";
            echo "<td id='seria_$idVaccine'>$seria</td>";
            echo "<td id='quantity_$idVaccine'>$quantity</td>";
            echo "<td id='vakDate_$idVaccine'>$vakDate</td>";
            echo "<td id='vakType_$idVaccine'><input type='hidden' id='vakTypeVal_$idVaccine' value='$vakType'>";
            foreach ($vakTypesList as $vak){
                echo ($vak['id'] == $vakType ? $vak['name'] : "");
            }
            echo "</td>";
            echo "<td id='denyCode_$idVaccine'><input type='hidden' id='vakDenyVal_$idVaccine' value='$denyCode'>";
            foreach ($denyCodesList as $deny){
                echo ($deny['id'] == $denyCode ? $deny['name'] : "");
            }
            echo "</td>";
            echo "<td id='comment_$idVaccine'>";
            echo ($comment ? $comment : '');
            echo "</td>";
            ?>
            <td>
                <input type='image' name="deleteVaccine" src='//172.16.0.35/cehovay/img/delete.ico' onclick="showModalWin('ConfirmDelete');" style='height:16px; width: 16px' value='Удалить'>
            </td>
        </tr>
        <?php
        unset($seria, $quantity, $vakDate, $vakCode, $vakType, $denyCode, $comment);
    }
    ?>
    </tbody>
</table>