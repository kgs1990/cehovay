<?php
/**
 * Created by PhpStorm.
 * User: kukuruz-gs
 * Date: 06.04.2018
 * Time: 21:07
 */
?>

<form method='post'>
        <h3 id='roentgenModalWin'></h3>
        <input type='hidden' id='editRoentgenMode' name="editRoentgenMode">
        <input type='hidden' id="idRowRoentgenHidden" name="idRowRoentgenHidden">
        <table class='table'>
            <tr>
                <td>Уник. номер</td>
                <td><input id="idRowRoentgen" type='text' disabled></td>
            </tr>
            <tr>
                <td>Дата</td>
                <td><input id="editDateRoentgen" name="editDateRoentgen" type='date'></td>
            </tr>
            <tr>
                <td>Результат</td>
                <td><input id="editResultRoentgen" name="editResultRoentgen" type="text"></td>
            </tr>
        </table>
        <input type="submit" class='btn' id="editRoentgenDone" name="editRoentgenDone" value="Готово">
</form>