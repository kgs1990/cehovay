<?php
/**
 * Created by PhpStorm.
 * User: kukuruz-gs
 * Date: 09.04.2018
 * Time: 0:34
 */
?>

<table  class='table table-hover'>
    <thead>
    <tr>
        <th><input type='image' id='insertAnalysis' onclick="insertAnalysisRow()" src='//172.16.0.35/cehovay/img/notebookplus.ico' style='height:16px; width: 16px' value='Добавить'></th>
        <th>у/н</th>
        <th>Вич</th>
        <th>Фактор</th>
        <th>Гепатит С</th>
        <th>Фактор</th>
        <th>Гепатит B</th>
        <th>Фактор</th>
    </tr>
    </thead>
    <tbody>
    <? foreach($analyses as $analysis) { ?>
    <tr id="<?= $analysis['id']; ?>" onclick="editAnalysisRow(this.id); deleteRow(this.id, 'analysis');">
        <td><input type='image' onclick="showModalWin('analysisEdit')" src='//172.16.0.35/cehovay/img/217485.png' style='height:16px; width: 16px' value='Редактировать'></td>
        <td id="analysisID_<?= $analysis['id']; ?>"><?= $analysis['id']?></td>
        <td id="hiv_<?= $analysis['id']?>"><?= $analysis['hiv']?></td>
        <td id="hivFactor_<?= $analysis['id']?>"><?= $analysis['hivFactor']?></td>
        <td id="hepatitis_c_<?= $analysis['id']?>"><?= $analysis['hepatitis_c']?></td>
        <td id="factor_c_<?= $analysis['id'] ?>"><?= $analysis['factor_c'] ?></td>
        <td id="hepatitis_b_<?= $analysis['id'] ?>"><?= $analysis['hepatitis_b'] ?></td>
        <td id="factor_b_<?= $analysis['id'] ?>"><?= $analysis['factor_b'] ?></td>
        <td><input type='image' name='' src='//172.16.0.35/cehovay/img/delete.ico' onclick="showModalWin('ConfirmDelete');" style='height:16px; width: 16px' value='Удалить'></td>
        </tr><? } ?>
    </tbody>
</table>
