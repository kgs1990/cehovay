<?PHP 
	header("Content-Type: text/html; charset=utf-8");
	include "connection.php";
	include "au.php";
	include "action.php";
    include "sql/sqlQuery.php";
?>

<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<title>Карта пациента</title>
</head>
<body style="margin: 1% 5% 1% 5%">
	<div id="conteiner">
		<?php include 'header.php' ?>
	</div>
		<br>
	<input type="button" name="edit" value="Редактировать" onclick="showModalWin('popupWin')" class="btn" style="float: left;">
		<br><br>

<table class='table table-striped'>
	<tr>
		<td class='bold'>Карта</td>
		<td class='pacColor'><?= $card ?></td>
	</tr>
	<tr>
		<td class='bold'>ФИО</td>
		<td class='pacColor'><?= $name ?></td>
	</tr>
	<tr>
		<td class='bold'>Дата рождения</td>
		<td class='pacColor'><?= $strBirth ?></td>
	</tr>
	<tr>
		<td class='bold'>Пол</td>
		<td class='pacColor'><?= $sex ?></td>
	</tr>
	<tr>
		<td class='bold'>Населенный пункт</td>
		<td class='pacColor'><?= $settlement ?></td>
	</tr>
	<tr>
		<td class='bold'>Адрес</td>
		<td class='pacColor'><?= $adress ?></td>
	</tr>
	<tr>
		<td class='bold'>Полис</td>
		<td class='pacColor'><?= $policy ?></td>
	</tr>
	<tr>
		<td class='bold'>Должность</td>
		<td class='pacColor'><?= $specialist ?></td>
	</tr>
	<tr>
		<td class='bold'>Отделение</td>
		<td class='pacColor'><?= $division?></td>
	</tr>
</table>

<!-- Вкладки -->
<div id="tabs">
        <div class="tab whiteborder">Вакцины</div>
        <div class="tab">R-графия</div>
        <div class="tab">Анализы</div>
        <!-- Вакцины -->
        <div class="tabContent"><?  include('php/vaccine.php'); ?></div>
        <!-- R-графия -->
        <div class="tabContent"><? include('php/roentgen.php'); ?></div>
        <!-- Анализы -->
        <div class="tabContent"><? include('php/Analyses.php'); ?></div>
</div>

    <!-- Модальное окно редактирования вакцин-->
    <div style="text-align: left;" id="editVakWin"  class="modalwin"><? include('php/editVaccine.php'); ?></div>


    <!-- Модальное окно Да-Нет -->
    <div id="ConfirmDelete"  class="modalwin">
        <?php include('html/confirmForm.html'); ?>
    </div>
        
<!-- Модальное окно -->
<div style="text-align: left;" id="popupWin"  class="modalwin"><? include('php/editPacient.php'); ?></div>

<!-- модальное окно редактивоания рентгенов -->
<div id='roentgenEdit' class='modalwin'><? include("php/editRoentgen.php"); ?></div>

<!-- модальное окно редактивоания анализов -->
<div id='analysisEdit' class='modalwin'><? include("php/editAnalysis.php"); ?></div>

<?php 
	// процедура обновления данных в базе
	function upData($connect, $idRec){

		$sql = "UPDATE регистр 
		SET карты = '%d',
		ФИО = '%s',
		пол = '%s',
		дата_рожд = '%s',
		полиса = '%s',
		н_пункт = '%d',
		адрес = '%s',
		отделение = '%d',
		должность = '%d'
		WHERE код_сотрудника = '%s'";

		$card = $_POST['card'];
		$name = $_POST['name'];
		$sex = $_POST['sex'];
		$birth = strtotime($_POST['birth']);
		$birth = date('d-m-Y', $birth); 
		$spec = $_POST['specialist'];
		$divis = $_POST['division'];
		$settl = $_POST['settlement'];
		$adress = $_POST['adress'];
		$polis = $_POST['policy'];

		$query = sprintf($sql, $card, $name, $sex, $birth, $polis, $settl, $adress, $divis, $spec, $idRec);
		echo $query;
		$result = sqlsrv_query($connect, $query);
 		
 		if (!$result) die ('При обновлении данных сотрудника произошла ошибка!');
	}

	// функция обновляет страницу со всеми get и post переменными
	function refresh($url=NULL){
        if(empty($url)){$url="{$_SERVER['REQUEST_URI']}";}
        die("<meta http-equiv='refresh' content=0; url=".$url."'>");
    }	

	// если нажата кнопка 'action', тогда обновить данные
	if (isset($_POST['action'])){
		upData($connect, $idRec);
		refresh('');
	}

?>

<script src="//172.16.0.35/cehovay/js/jquery.min.js"></script>
<script src='//172.16.0.35/cehovay/js/script.js'></script>

</body>
</html>