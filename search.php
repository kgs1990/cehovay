<?PHP 
	header("Content-Type: text/html; charset=utf-8");
	include "connection.php";
	include "au.php";
?>

<html>
<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" /> 
	<title>Поиск</title>
</head>
<body style="margin: 1% 5% 1% 5%">
	<div id="conteiner">
		<?php include 'header.php' ?>
	</div>
	<br>

	<form id="find" name="find" action="search.php" method="POST">
			<h4><label for="pac">Найти: </label></h4>
            <table>
                <tr><p><input id="pac" type="text" name="find_string" class="form-control"></p></tr>
                <tr>
                    <td style='padding-right: 5px'><p><input id="btn" type="submit" name="finder" value="Найти" class="form-control"></p></td>
                    <td><p><input id="btn" type="button" name="newWorker" onclick="window.location='http://google.com';" value="Новый" class="form-control"></p></td>
                </tr>
            </table>
	</form>

	<?php
	if (isset($_POST['finder'])){
		$query ='SELECT карты as card, код_сотрудника as numb, ФИО as Name, Subdivision, specializit
		FROM регистр AS t1 LEFT OUTER JOIN подразделения AS t2 ON t1.отделение = t2.Idsubdivision
		LEFT OUTER JOIN должности AS t3 ON t1.должность = t3.Idspecializit
		WHERE ФИО LIKE \'%'.strip_tags($_POST['find_string']).'%\' OR Subdivision LIKE \'%'.strip_tags($_POST['find_string']).'%\'
		ORDER BY ФИО';

		$result = sqlsrv_query($connect, $query);
	?>
	<table class="table table-hover">
	<thead>
		<tr>
			<th>Карта</th>
			<th>ФИО</th>
			<th>Отделение</th>
			<th>Должность</th>
		</tr>
	</thead>
	<tbody>
	<?php
		while($row = sqlsrv_fetch_array($result, SQLSRV_FETCH_ASSOC))
		{ 
	?>
	<tr>
	<?php
		echo "<td>".$row['card']."</td>";
		echo "<td><a href=\"pacient.php/?id=".$row['numb']."\" target=\"_blank\">".$row['Name']."</a></td>"; 
		echo "<td>".$row['Subdivision']."</td>";
		echo "<td>".$row['specializit']."</td>";
	?>
	</tr>
	<?php
		}
	}
	?>
	</tbody>
	</table>
	
	<?php
		/* Close the connection. */
		sqlsrv_close( $connect);
	?>
</body>
</html>