<?php
 /*
 * User: kgs19
 * Date: 19.02.2019
 * Time: 13:24
 */

$uploaddir = $_SERVER['DOCUMENT_ROOT']."/uptest/";
@mkdir($uploaddir, 0777);
$uploadfile = $uploaddir . basename($_FILES['upload']['name']);

echo '<pre>';
if (move_uploaded_file($_FILES['upload']['tmp_name'], $uploadfile)) {
    echo "Файл корректен и был успешно загружен.\n";
} else {
    echo "Возможная атака с помощью файловой загрузки!\n";
}

echo 'Некоторая отладочная информация:';
print_r($_FILES);

print "</pre>";

?>

<form action="" method="post" enctype="multipart/form-data">
    <p>Изображения:
        <input type="file" name="upload" />
        <input type="submit" value="Отправить" />
    </p>
</form>
