	// модальное окно
	function showModalWin(str) {

		var darkLayer = document.createElement('div'); // слой затемнения
		darkLayer.id = 'shadow'; // id чтобы подхватить стиль
		document.body.appendChild(darkLayer); // включаем затемнение
		//'popupWin'
		var modalWin = document.getElementById(str); // находим наше "окно"
		modalWin.style.display = 'block'; // "включаем" его
		 
		darkLayer.onclick = function () {  // при клике на слой затемнения все исчезнет
			darkLayer.parentNode.removeChild(darkLayer); // удаляем затемнение
			modalWin.style.display = 'none'; // делаем окно невидимым
			return false;
		};
	}

	// Вкладки
	var tab; // заголовок вкладки
	var tabContent; // блок содержащий контент вкладки


	window.onload = function() {
	    tabContent = document.getElementsByClassName('tabContent');
	    tab = document.getElementsByClassName('tab');
	    hideTabsContent(1);
	}

	document.getElementById('tabs').onclick= function (event) {
	    var target = event.target;
	    if (target.className == 'tab') {
	       for (var i=0; i<tab.length; i++) {
	           if (target == tab[i]) {
	               showTabsContent(i);
	               break;
	           }
	       }
	    }
	}

	function hideTabsContent(a) {
	    for (var i=a; i<tabContent.length; i++) {
	        tabContent[i].classList.remove('show');
	        tabContent[i].classList.add("hide");
	        tab[i].classList.remove('whiteborder');
	    }
	}

	function showTabsContent(b){
	    if (tabContent[b].classList.contains('hide')) {
	        hideTabsContent(0);
	        tab[b].classList.add('whiteborder');
	        tabContent[b].classList.remove('hide');
	        tabContent[b].classList.add('show');
	    }
	}

	/*
	* генерирует заданную ссылку с указанными параметрами
    function getRowIdInTable(idPac, idRow) {
        location.href = '?id=' + idPac + '&row=' + idRow;
    }
	*/

	function editVaccineRow(idRow){
        document.getElementById('VaccineWindowMode').value = 'edit';
        document.getElementById('vaccineModeTitle').innerHTML = 'Редактировать запись';
        document.getElementById('rowID').value = idRow;
		document.getElementById('editVaccineCode').value = document.getElementById('vakCodeVal_' + idRow).value;
        document.getElementById('editSeriaVaccine').value = document.getElementById('seria_' + idRow).innerHTML;
        document.getElementById('editQuantityVaccine').value = document.getElementById('quantity_' + idRow).innerHTML;
        document.getElementById('editDateVaccine').value = document.getElementById('vakDate_' + idRow).innerHTML;
        document.getElementById('editVaccineType').value = document.getElementById('vakTypeVal_' + idRow).value;
        document.getElementById('editVaccineDeny').value = document.getElementById('vakDenyVal_' + idRow).value;
        document.getElementById('editVaccineComment').value = document.getElementById('comment_' + idRow).innerHTML;
    }

    function  editRoentgenRow(idRow) {
		document.getElementById('editRoentgenMode').value = 'edit';
		document.getElementById('idRowRoentgen').value = document.getElementById('roentgenID_' + idRow).innerHTML;
        document.getElementById('idRowRoentgenHidden').value = document.getElementById('roentgenID_' + idRow).innerHTML;
		document.getElementById('editDateRoentgen').value = document.getElementById('roentgenDate_' + idRow).innerHTML;
		document.getElementById('editResultRoentgen').value = document.getElementById('roentgenComment_' + idRow).innerHTML;
    }

    function  editAnalysisRow(idRow) {
		document.getElementById('editAnalysisMode').value = 'edit';
		document.getElementById('idRowAnalysis').value = document.getElementById('analysisID_' + idRow).innerHTML;
		document.getElementById('idRowAnalysisHidden').value = document.getElementById('analysisID_' + idRow).innerHTML;
        document.getElementById('editHiv').value = document.getElementById('hiv_' + idRow).innerHTML;
		document.getElementById('editFactorHiv').value = document.getElementById('hivFactor_' + idRow).innerHTML;
		document.getElementById('editHepatitisC').value = document.getElementById('hepatitis_c_' + idRow).innerHTML;
		document.getElementById('editFactorC').value = document.getElementById('factor_c_' + idRow).innerHTML;
		document.getElementById('editHepatitisB').value = document.getElementById('hepatitis_b_' + idRow).innerHTML;
		document.getElementById('editFactorB').value = document.getElementById('factor_b_' + idRow).innerHTML;
    }

    function insertVaccineRow(){
        document.getElementById('VaccineWindowMode').value = 'insert';
		document.getElementById('vaccineModeTitle').innerHTML = 'Добавить запись';
        document.getElementById('rowID').value = '';
		document.getElementById('editVaccineCode').value = '';
        document.getElementById('editSeriaVaccine').value = '';
        document.getElementById('editQuantityVaccine').value = '';
        document.getElementById('editDateVaccine').value = '';
        document.getElementById('editVaccineType').value = '';
        document.getElementById('editVaccineDeny').value = '';
        document.getElementById('editVaccineComment').value = '';
        showModalWin('editVakWin');
    }

    function insertRoentgenRow(){
        document.getElementById('editRoentgenMode').value = 'insert';
        document.getElementById('idRowRoentgenHidden').value = '';
        document.getElementById('idRowRoentgen').value = '';
        document.getElementById('editDateRoentgen').value = '';
        document.getElementById('editResultRoentgen').value = '';
        showModalWin('roentgenEdit');
	}

	function insertAnalysisRow(){
        document.getElementById('editAnalysisMode').value = 'insert';
        showModalWin('analysisEdit');
	}

    function deleteRow(idRow, rowType){
    	document.getElementById('deleteRowType').value = rowType;
		var rowID = document.getElementById('deleteRowByID');
		rowID.value = idRow;
	}

    function onKeyPress(e){
		var ev = e.event;
		if (ev.keyCode == 13){
			alert('111');
		}
		var parenRow = e.parentNode.parentNode;
		alert(e.name);
	}

	function showField(e){

	}