<?php
/**
 * Created by PhpStorm.
 * User: kukuruz-gs
 * Date: 15.03.2018
 * Time: 22:28
 */

// сохранение данных о вакцинации
    if (isset($_POST['editVaccineDone']) and ($_POST['VaccineWindowMode'] == 'edit')){
    $sql = "UPDATE вакцины SET код_вак = " . $_POST['editVaccineCode']
        . ", серия = '" . $_POST['editSeriaVaccine'] . "'"
        . ", колич = '" . $_POST['editQuantityVaccine'] . "'"
        . ", дата =  '%s'"
        . ", вид_при = " . $_POST['editVaccineType']
        . (isset($_POST['editVaccineDeny']) ? ", код_пом_отказ = '" . $_POST['editVaccineDeny'] . "'" : "")
        . ", прим = '" . $_POST['editVaccineComment'] . "'"
        . "WHERE код_вакцинации = " . $_POST['rowID'] ;
    $dateVaccine = strtotime($_POST['editDateVaccine']);
    $dateVaccine = date('d-m-Y', $dateVaccine);
    $query = sprintf($sql, $dateVaccine);
    sqlsrv_query($connect, $query) or die('Не удалось обновить данные...');
    } elseif (isset($_POST['editVaccineDone']) and ($_POST['VaccineWindowMode'] == 'insert')){
        $uid = (isset($_GET['id'])) ? $_GET['id'] : 'null';
        $seria = (isset($_POST['editSeriaVaccine'])) ? $_POST['editSeriaVaccine'] : '';
        $quantity = (isset($_POST['editQuantityVaccine'])) ? $_POST['editQuantityVaccine'] : '';
        $dateVaccine = strtotime($_POST['editDateVaccine']);
        $dateVaccine = date('d-m-Y', $dateVaccine);
        $code = (isset($_POST['editVaccineCode'])) ? $_POST['editVaccineCode'] : 'null';
        $type = (isset($_POST['editVaccineType'])) ? $_POST['editVaccineType'] : 'null';
        $deny = (isset($_POST['editVaccineDeny'])) ? $_POST['editVaccineDeny'] : 'null';
        $comment = (isset($_POST['editVaccineComment'])) ? $_POST['editVaccineComment'] : '';
        $sql = "INSERT INTO вакцины (код_сотрудника, серия, колич, дата, код_вак, вид_при, код_пом_отказ, прим)
                VALUES ('{$uid}'
                , '{$seria}'
                , '{$quantity}'
                , {$dateVaccine}
                , {$code}
                , {$type}
                , {$deny}
                , '{$comment}')";
        sqlsrv_query($connect, $sql) or die('Не удалось добавить данные... ' .  print_r( sqlsrv_errors(), true));
        unset($seria, $quantity,  $dateVaccine, $code, $type, $deny, $comment);
    }

    // удаление строк
    if(isset($_POST['deleteRowType']) && isset($_POST['deleteRowByID'])){
        if ($_POST['deleteRowType'] == 'vaccine' && $_POST['confirmDeleteYesNo'] == 'Да'){
            $sql = "DELETE FROM вакцины WHERE код_вакцинации = {$_POST['deleteRowByID']}";
            sqlsrv_query($connect, $sql);
        } elseif ($_POST['deleteRowType'] == 'roentgen' && $_POST['confirmDeleteYesNo'] == 'Да'){
            $sql = "DELETE FROM флюрография WHERE код_ф = {$_POST['deleteRowByID']}";
            sqlsrv_query($connect, $sql);
        } elseif ($_POST['deleteRowType'] == 'analysis' && $_POST['confirmDeleteYesNo'] == 'Да'){
            $sql = "DELETE FROM анализы WHERE код_анализа = {$_POST['deleteRowByID']}";
            sqlsrv_query($connect, $sql);
        }
    }

// сохраняем данные о рентгенах
    if (isset($_POST['editRoentgenDone']) AND ($_POST['editRoentgenMode'] == 'edit')) {
        $sql = "UPDATE флюрография SET дата_ф = '%s',
        примечание = '%s' WHERE код_ф = {$_POST['idRowRoentgenHidden']}";
        $dateRoentgen = strtotime( $_POST['editDateRoentgen']);
        $dateRoentgen = date('d-m-Y', $dateRoentgen);
        $query = sprintf($sql, $dateRoentgen, $_POST['editResultRoentgen']);
        sqlsrv_query($connect, $query) or die('Не удалось обновить данные...');
    } elseif (isset($_POST['editRoentgenDone']) AND ($_POST['editRoentgenMode'] == 'insert')){
        $sql = "INSERT INTO флюрография (код_сотрудника, дата_ф, примечание)
                VALUES ('%d', '%s', '%s')";
        $dateRoentgen = strtotime($_POST['editDateRoentgen']);
        $dateRoentgen = date('d-m-Y', $dateRoentgen);
        $query = sprintf($sql, $_GET['id'], $dateRoentgen, $_POST['editResultRoentgen']);
        sqlsrv_query($connect, $query) or die('Не удалось добавить данные...');
    }

// сохраняем данные о исследованиях
    if (isset($_POST['editAnalysisDone']) AND ($_POST['editAnalysisMode'] == 'edit')) {
        $sql = "UPDATE анализы SET вич = '%s'
        , ФАКТОР_ВИЧ = '{$_POST['editFactorHiv']}'
        , гепатит_с = '%s'
        , фактор_с = '{$_POST['editFactorC']}'
        , гепатит_в = '%s'
        , Фактор_в = '{$_POST['editFactorB']}'
        WHERE код_анализа =  {$_POST['idRowAnalysisHidden']}";
        $hiv = strtotime($_POST['editHiv']);
        $hiv = date('d-m-Y', $hiv);
        $HepatitisC = strtotime($_POST['editHepatitisC']);
        $HepatitisC = date('d-m-Y', $HepatitisC);
        $HepatitisB = strtotime($_POST['editHepatitisB']);
        $HepatitisB = date('d-m-Y', $HepatitisB);
        $query = sprintf($sql, $hiv, $HepatitisC, $HepatitisB);
        sqlsrv_query($connect, $query) or die(print_r( sqlsrv_errors(), true));
    } elseif (isset($_POST['editAnalysisDone']) AND ($_POST['editAnalysisMode'] == 'insert')) {
        $sql = "INSERT INTO анализы (код_сотрудника, вич, ФАКТОР_ВИЧ, гепатит_с, фактор_с, гепатит_в, Фактор_в)
                VALUES ('%d', '%s', '%s', '%s', '%s', '%s', '%s')";
        $hiv = strtotime($_POST['editHiv']);
        $hiv = date('d-m-Y', $hiv);
        $HepatitisC = strtotime($_POST['editHepatitisC']);
        $HepatitisC = date('d-m-Y', $HepatitisC);
        $HepatitisB = strtotime($_POST['editHepatitisB']);
        $HepatitisB = date('d-m-Y', $HepatitisB);
        $query = sprintf($sql, $_GET['id'], $hiv, $_POST['editFactorHiv'], $HepatitisC, $_POST['editFactorC'], $HepatitisB, $_POST['editFactorB']);
        sqlsrv_query($connect, $query) or die(print_r( sqlsrv_errors(), true));
    }